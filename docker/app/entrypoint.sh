#!/bin/bash
set -e
export MIX_ENV=prod
if [ "$INIT" = TRUE ]; then
	echo "Initialization. Only run this once."
	mix ecto.migrate 
	mix mobilizon.users.new "$ADMIN_EMAIL" --admin --password "$ADMIN_PASSWORD" 
	mix phx.server 
else
	echo "Running normal mode."
	mix ecto.migrate --no-deps-check
	mix phx.server --no-deps-check
fi

exec "$@"
